@extends('layout.master')
@section('title')
Halaman List Cast
@endsection

@section ('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Create Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                  <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                  @method('delete')
                  <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail Data</a>
                  <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit Data</a>
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
                </td>
            </tr>
        @empty
            <tr>
               <td>Belum Ada Data</td> 
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection