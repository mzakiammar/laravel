@extends('layout.master')
@section('title')
Halaman Cast
@endsection

@section ('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input name="nama" type="text" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input name="umur" type="text" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection