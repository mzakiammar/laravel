<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@utama');
Route::get('/register', 'AuthController@daftar');
Route::post('/welcome', 'AuthController@sambutan');
Route::get('/table', 'TableController@table');

Route::get('/data-tables', function(){
    return view('page.dataTable');
});

//CRUD cast

//Membuat Data Cast
Route::get('/cast/create', 'CastControllers@create');
Route::post('/cast', 'CastControllers@store');

//Read Data Cast
Route::get('/cast', 'CastControllers@index');
Route::get('/cast/{$cast_id}', 'CastControllers@show');

//Update Data Cast
Route::get('/cast/{cast_id}/edit', 'CastControllers@edit');
Route::put('/cast/{cast_id}', 'CastControllers@update');

//Hapus Data Cast
Route::delete('/cast/{cast_id}', 'CastControllers@destroy');