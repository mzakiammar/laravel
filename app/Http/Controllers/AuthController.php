<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.form');
    }

    public function sambutan(Request $request)
    {
        $namaDepan = $request['namaD'];
        $namaBelakang = $request['namaB'];

        return view('page.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
